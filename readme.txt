Procedure followed:

1.	Connection of Jenkins with Git (only required when the code repo is private. Can skip this step as the repo provided was public)
		-Create credential "devops_test" in the jenkins for the SCM pull:
		-Credential type: SSH username with priivate key
		-Add public key of your jenkins master in the git repository.


2.	Connection of Jenkins with build server:
		-Crete a node agent "devops_test_node" giving the credentials of build machine in same.

3.	Pipeline configuration:
		-created a pipeline project in jenkins: sahuja_test by clicking on new > pipeline project
		-Configuration: added a string parameter: BUILD_PATH
		-Under Build trigger section , check marked "GitHub hook trigger for GITScm polling" to ensure build trigger on every push. You can aslos add a webhook for jenkins in your GIT in the fotrmat "https://USERNAME:PASSWORD@<jenkins_server>/github-webhook/ for Jenkins to get notification from Git whenever there is a new commit.
		-In pipeline section: selected Pipeline script from SCM
			SCM: Git
			Repo URL: https://gitlab.com/kagarwal0205/sampleproject.git (the public repo provided)
			credential: devops_test (required only when the above repo is private, can skip here)
			branch: master
			Script path: Jenkinsfile (the file I have created)



PS: I was not sure whether you wanted me to build the jar too. 
If that is the case then I would have used the eclipse plugin in my pipeline to build the java code, generate a jar and then the deployment would have been in the same manner I have done in the present scenerio.
I didn't have mch time, prepared this code in 2 hours.
